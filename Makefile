.DEFAULT_GOAL := all

CC= gcc
TARGET = test_bitwisetrie
BUILD_DIR=./

CFLAGS = -g -Wall -Werror -I$(CURDIR)/
WARNING := -Wall -Wextra

SOURCES= \
	bitwise_trie.c \
	bitwise_trie.h \
	test/test_main.c \
	test/test_main.h \
	test/test_create.c \
	test/test_add.c \
	test/test_spend.c \
	test/test_second_root.c \
	test/test_trim.c

OBJECTS := $(patsubst %.c,%.o,$(SOURCES))

%.o: %.c Makefile
	$(CC) $(WARNING) $(CFLAGS) -MMD -MP -c $< -o $@


$(TARGET): $(OBJECTS)
	$(CC) $(WARNING) $(CFLAGS) $^ -o $@


all: $(TARGET)


.PHONY : clean
clean:
	rm $(TARGET)
	rm ./*.d
	rm ./*.o
	rm ./test/*.d
	rm ./test/*.o
