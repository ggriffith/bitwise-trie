# Bitwise Trie

Source code is located at https://gitlab.com/ggriffith/bitwise-trie

Code style guide for C code:
- All macros and constants in caps: MAX_ROOTS_IN_HISTORY, DEFAULT_NODE_MASTER_LIST_SIZE.
- Struct names in camelcase: BitwiseTrie, TrieNode.
- typedefs in lower case and underscore to match other native types: uint256_t.
- Functions that operate on structs begin with camelcase struct name then underscore all lower case function name: BitwiseTrie_create, Uint256_to_string
- Pointers: TrieNode *node.
- Functions that do not operate on structs are all lower case and underscore: test_adds.
- Functions that are not meant to be called directly begin with underscore and follow other function naming conventions: _BitwiseTrie_add_node_to_master_list
- variables are all lower case and underscore
