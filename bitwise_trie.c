// Copyright (c) 2022 Dr. Peter R. Rizun
// Copyright (c) 2022 Greg Griffith
// Copyright (c) 2022 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "bitwise_trie.h"

// Notes:
// - memcpy is less expensive than memset when initialising uint256_t arrays to 0
// -

// we want to "inline" the BIN variable definitions using
// preprocessor macros (define) to prevent requiring the program to
// fetch the value of the BIN numbers from the initialised data segment
// of memory when they are needed. The macros put the value on the stack
// instead, which is cheaper

#define BIN_00000000 0
#define BIN_00000001 1
#define BIN_00000010 2
#define BIN_00000100 4
#define BIN_00001000 8
#define BIN_00010000 16
#define BIN_00100000 32
#define BIN_01000000 64
#define BIN_10000000 128

#define BIN_01111111 127
#define BIN_00111111 63
#define BIN_00011111 31
#define BIN_00001111 15
#define BIN_00000111 7
#define BIN_00000011 3

#define BIN_11111111 255

#define BIN_11000000 192
#define BIN_11100000 224
#define BIN_11110000 240
#define BIN_11111000 248
#define BIN_11111100 252
#define BIN_11111110 254


static void _BitwiseTrie_update_memory_allocated(BitwiseTrie* trie, const int64_t change)
{
    if (change < 0)
    {
        assert(trie->mem_used > labs(change)); // labs = absolute value return type of long
    }
    trie->mem_used = trie->mem_used + change;
}

static void _TrieNode_destroy(TrieNode* node)
{
    if (node->value)
    {
        if (node->value->len_scriptpubkey != 0)
        {
            free(node->value->scriptpubkey);
        }
        free(node->value);
    }
    // do not free the left, right, or parent.
    // they are freed elsewhere
    free(node);
}

static bool _TrieNode_create_null_value(TrieNode* node)
{
    node->value = malloc(sizeof(TrieEntry));
    if (node->value == NULL)
    {
        return false;
    }
    node->value->amount = 0;
    node->value->len_scriptpubkey = 0;
    node->value->scriptpubkey = NULL;
    return true;
}

// a null node is a node with no other values and a root group of the current root index
static void _TrieNode_set_null(TrieNode* node, const BitwiseTrie* trie)
{
    node->parent = NULL;
    node->left = NULL;
    node->right = NULL;
    node->root_group = INVALID_ROOT_GROUP;
    // memcpy, not malloc, uint256_t values are statically sized 32 byte arrays on the stack.
    memcpy(node->key, UINT256_ZERO, UINT256_NUM_BYTES);
    node->key_bits = 0;
    memcpy(node->fingerprint, UINT256_ZERO, UINT256_NUM_BYTES);
    node->value = NULL;
    node->root_group = trie->active_root_index;
}

static void _BitwiseTrie_set_root_prefix(TrieNode* root, const uint8_t* prefix, const size_t len_prefix_bits)
{
    size_t bytes_to_copy = 1;
    if (prefix != NULL)
    {
        size_t last_bit = len_prefix_bits % 8;
        if (last_bit == 0) // ended on a full byte
        {
            bytes_to_copy = len_prefix_bits / 8;
            memcpy(root->key, prefix, bytes_to_copy); // copy the prefix to the first bytes of the key
            // arrays are 0 index, the index of bytes_to_copy is the next byte after the provided prefix.
            root->key[bytes_to_copy] = BIN_10000000;
        }
        else
        {
            bytes_to_copy = (len_prefix_bits / 8) + 1;
            memcpy(root->key, prefix, bytes_to_copy); // copy the prefix to the first bytes of the key
            // figure out which bit was the last one specified by the provided prefix
            // and set the following bit to 1
            if (last_bit == 7)
            {
                root->key[bytes_to_copy - 1] &= BIN_00000001;
            }
            else if (last_bit == 6)
            {
                root->key[bytes_to_copy - 1] &= BIN_00000010;
            }
            else if (last_bit == 5)
            {
                root->key[bytes_to_copy - 1] &= BIN_00000100;
            }
            else if (last_bit == 4)
            {
                root->key[bytes_to_copy - 1] &= BIN_00001000;
            }
            else if (last_bit == 3)
            {
                root->key[bytes_to_copy - 1] &= BIN_00010000;
            }
            else if (last_bit == 2)
            {
                root->key[bytes_to_copy - 1] &= BIN_00100000;
            }
            else if (last_bit == 1)
            {
                root->key[bytes_to_copy - 1] &= BIN_01000000;
            }

        }
        root->key_bits = len_prefix_bits + 1;
    }
    else
    {
        root->key[0] = BIN_00000000;
        root->key_bits = 0;
    }
}

static int64_t _TrieNode_get_memusage(TrieNode* node)
{
    int64_t mem_change = sizeof(TrieNode);
    if (node->value != NULL)
    {
        mem_change += sizeof(TrieEntry);
        if(node->value->scriptpubkey != NULL)
        {
            mem_change += node->value->len_scriptpubkey;
        }
    }
    return mem_change;
}

static void _BitwiseTrie_add_node_to_master_list(BitwiseTrie* trie, TrieNode* node)
{
    _BitwiseTrie_update_memory_allocated(trie, _TrieNode_get_memusage(node));
    trie->node_master_list[trie-> len_node_master_list] = node;
    ++trie-> len_node_master_list;
}

BitwiseTrie* BitwiseTrie_create_with_prefix(const uint8_t* prefix, const size_t len_prefix, void(*hash_function)(const uint256_t a, const uint256_t b, uint256_t res))
{
    // check the given prefix before anything else
    if (prefix == NULL && len_prefix > 0)
    {
        // invalid prefix values, return null
        return NULL;
    }
    if (prefix != NULL && len_prefix == 0)
    {
        return false;
    }
    if (prefix != NULL)
    {
        if (len_prefix >= UINT256_NUM_BYTES)
        {
            // invalid prefix length, must leave at least 1 byte available for key values
            return NULL;
        }
    }
    BitwiseTrie* new_trie = malloc(sizeof(BitwiseTrie));
    // check malloc
    if (new_trie == NULL)
    {
        // failed to malloc memory, return null to indicate failure
        return NULL;
    }
    // create the trie root
    // normally we would validate the position of the root before adding a new one
    // but we know we have space when creating a new trie
    new_trie->current_root = malloc(sizeof(TrieNode));
    if (new_trie->current_root == NULL)
    {
        // failed to malloc memory, return null to indicate failure
        free(new_trie);
        return NULL;
    }
    // set root to null
    _TrieNode_set_null(new_trie->current_root, new_trie);
    // make master node list
    new_trie->node_master_list = malloc(sizeof(TrieNode*) * DEFAULT_NODE_MASTER_LIST_SIZE);
    if (new_trie->node_master_list == NULL)
    {
        // failed to malloc memory, return null to indicate failure
        _TrieNode_destroy(new_trie->current_root);
        free(new_trie);
        return NULL;
    }
    // we could calloc above rather than memset here but it is slightly more expensive to do so
    memset(new_trie->node_master_list, 0, (sizeof(TrieNode*) * DEFAULT_NODE_MASTER_LIST_SIZE));
    // set the root prefix
    _BitwiseTrie_set_root_prefix(new_trie->current_root, prefix, len_prefix);
    // nullify all entries in root history
    memset(new_trie->historical_roots, 0, (sizeof(TrieNode*) * MAX_ROOTS_IN_HISTORY));
    // set root index in history
    new_trie->active_root_index = 0;
    new_trie->current_root->root_group = 0;
    new_trie->historical_roots[new_trie->active_root_index] = new_trie->current_root;
    new_trie->len_node_master_list = 0;
    new_trie->capacity_node_master_list = DEFAULT_NODE_MASTER_LIST_SIZE;
    new_trie->hash = hash_function;
    // update memory used at the end to avoid undoing memory allocation changes due to
    // malloc failures
    _BitwiseTrie_update_memory_allocated(new_trie, sizeof(BitwiseTrie)); // trie itself
    _BitwiseTrie_update_memory_allocated(new_trie, sizeof(TrieNode)); // root node
    _BitwiseTrie_update_memory_allocated(new_trie, (sizeof(TrieNode*) * DEFAULT_NODE_MASTER_LIST_SIZE)); // node list
    return new_trie;
}

BitwiseTrie* BitwiseTrie_create(void(*hash_function)(const uint256_t a, const uint256_t b, uint256_t res))
{
    return BitwiseTrie_create_with_prefix(NULL, 0, hash_function);
}

bool BitwiseTrie_add_new_root(BitwiseTrie* trie)
{
    if (trie->active_root_index == 63)
    {
        // historical roots is full, return false.
        return false;
    }
    TrieNode* new_root = malloc(sizeof(TrieNode));
    if (new_root == NULL)
    {
        // failed to malloc memory, return NULL to indicate failure to add node
        return false;
    }
    new_root->parent = trie->current_root->parent;
    new_root->left = trie->current_root->left;
    new_root->right = trie->current_root->right;
    new_root->value = trie->current_root->value;
    ++trie->active_root_index;
    new_root->root_group = trie->active_root_index;
    trie->historical_roots[trie->active_root_index] = new_root;
    trie->current_root = new_root;
    _BitwiseTrie_update_memory_allocated(trie, sizeof(TrieNode));
    return true;
}

static TrieNode* _TrieNode_copy(TrieNode* node)
{
    // do all of the mallocs first
    TrieNode* node_copy = malloc(sizeof(TrieNode));
    if (node_copy == NULL)
    {
        // failed to malloc memory, return NULL to indicate failure to add node
        return NULL;
    }
    if (node->value != NULL)
    {
        node_copy->value = malloc(sizeof(TrieEntry));
        if (node_copy->value == NULL)
        {
            // failed to malloc memory, return NULL to indicate failure to add node
            free(node_copy);
            return NULL;
        }
        node_copy->value->len_scriptpubkey = node->value->len_scriptpubkey;
        node_copy->value->amount = node->value->amount;
        if(node->value->scriptpubkey != NULL)
        {
            node_copy->value->scriptpubkey = malloc(node->value->len_scriptpubkey);
            if (node_copy->value->scriptpubkey == NULL)
            {
                // failed to malloc memory, return NULL to indicate failure to add node
                free(node_copy->value);
                free(node_copy);
                return NULL;
            }
            memcpy(node_copy->value->scriptpubkey, node->value->scriptpubkey, node->value->len_scriptpubkey);
        }
    }
    node_copy->key_bits = node->key_bits;
    memcpy(node_copy->key, node->key, UINT256_NUM_BYTES);
    node_copy->left = node->left;
    node_copy->right = node->right;
    node_copy->parent = node->parent;
    node_copy->root_group = node->root_group;
    return node_copy;
}

// makes a copy of node with the parent set to new_parent, the root group is set to the
// same root group as the parent
static TrieNode* _TrieNode_copy_with_new_parent(TrieNode* node, TrieNode* new_parent)
{
    TrieNode* node_copy = _TrieNode_copy(node);
    if (node_copy == NULL)
    {
        return NULL;
    }
    node_copy->parent = new_parent;
    node_copy->root_group = new_parent->root_group;
    return node_copy;
}

static bool _BitwiseTrie_check_and_expand_master_list(BitwiseTrie* trie)
{
    // if we do not have space for the node, expand the node list
    // len is 1 past the last stored, when it is equal to capacity the current allocated
    // memory is full
    if (trie->len_node_master_list == trie->capacity_node_master_list)
    {
        // double the list capacity
        TrieNode** new_node_master_list = malloc(sizeof(TrieNode*) * trie->capacity_node_master_list * 2);
        if (new_node_master_list == NULL)
        {
            return false;
        }
        // copy the old list into the new list
        memcpy(new_node_master_list, trie->node_master_list, (sizeof(TrieNode*) * trie->capacity_node_master_list));
        // free the old list
        free(trie->node_master_list);
        trie->node_master_list = new_node_master_list;
        // double the store capacity
        // no need to check for overflow, memory will run out before that happens
        trie->capacity_node_master_list *= 2;
        // add half the new capacity
        _BitwiseTrie_update_memory_allocated(trie, (sizeof(TrieNode*) * (trie->capacity_node_master_list / 2)));
    }
    return true;
}

static TrieNode* _TrieNode_make_new_interior_node(BitwiseTrie* trie, TrieNode* parent, TrieNode* replacing, const uint256_t key)
{
    TrieNode* interior_node = malloc(sizeof(TrieNode));
    if (interior_node == NULL)
    {
        // failed to malloc memory, return NULL to indicate failure to add node
        return NULL;
    }
    // set the new parent node to null
    _TrieNode_set_null(interior_node, trie);
    _BitwiseTrie_add_node_to_master_list(trie, interior_node);
    // copy key
    memcpy(interior_node->key, key, UINT256_NUM_BYTES);
    // calculate key_bits
    int32_t i = 0;
    while(memcmp(&key[i], &replacing->key[i], 1) == 0)
    {
        ++i;
    }
    interior_node->key_bits = 8 * i;
    // byte i is the differing byte, check the bits
    // do not check the last bit because this is an interior node
    uint8_t bin_number = BIN_00000000;
    uint8_t wild_bit_mask = BIN_00000000;
    if ((BIN_10000000 & key[i]) != (BIN_10000000 & replacing->key[i]))
    {
        interior_node->key_bits = interior_node->key_bits + 0;
        bin_number = BIN_10000000;
        wild_bit_mask = BIN_00000000;
    }
    else if ((BIN_01000000 & key[i]) != (BIN_01000000 & replacing->key[i]))
    {
        interior_node->key_bits = interior_node->key_bits + 1;
        bin_number = BIN_01000000;
        wild_bit_mask = BIN_10000000;
    }
    else if ((BIN_00100000 & key[i]) != (BIN_00100000 & replacing->key[i]))
    {
        interior_node->key_bits = interior_node->key_bits + 2;
        bin_number = BIN_00100000;
        wild_bit_mask = BIN_11000000;
    }
    else if ((BIN_00010000 & key[i]) != (BIN_00010000 & replacing->key[i]))
    {
        interior_node->key_bits = interior_node->key_bits + 3;
        bin_number = BIN_00010000;
        wild_bit_mask = BIN_11100000;
    }
    else if ((BIN_00001000 & key[i]) != (BIN_00001000 & replacing->key[i]))
    {
        interior_node->key_bits = interior_node->key_bits + 4;
        bin_number = BIN_00001000;
        wild_bit_mask = BIN_11110000;
    }
    else if ((BIN_00000100 & key[i]) != (BIN_00000100 & replacing->key[i]))
    {
        interior_node->key_bits = interior_node->key_bits + 5;
        bin_number = BIN_00000100;
        wild_bit_mask = BIN_11111000;
    }
    else if ((BIN_00000010 & key[i]) != (BIN_00000010 & replacing->key[i]))
    {
        interior_node->key_bits = interior_node->key_bits + 6;
        bin_number = BIN_00000010;
        wild_bit_mask = BIN_11111100;
    }
    else if ((BIN_00000001 & key[i]) != (BIN_00000001 & replacing->key[i]))
    {
        interior_node->key_bits = interior_node->key_bits + 7;
        bin_number = BIN_00000001;
        wild_bit_mask = BIN_11111110;
    }
    // 0 out all of the wild bits in the interior node
    // it is useful for later if they are all set to 0
    // setting them to 0 does not affect the shape of the trie
    int32_t k = i;
    interior_node->key[k] &= wild_bit_mask;
    ++k;
    for (; k < UINT256_NUM_BYTES; ++k)
    {
        interior_node->key[k] &= BIN_00000000;
    }
    // check if the first not key bit is 0 for left, 1 for right
    if ((replacing->key[i] & bin_number) == BIN_00000000)
    {
        interior_node->left = replacing;
        interior_node->right = NULL;
    }
    else
    {
        interior_node->right = replacing;
        interior_node->left = NULL;
    }
    // we can determine if this is replacing a parent's left or right node by the value of
    // the first wild bit
    // 0 is left, 1 is right
    // the exeption is when the parent is the root node
    if (parent->key_bits == 0)
    {
        // the parent is a root node but we do not know which one, grab the current root node
        if ((interior_node->key[0] & BIN_10000000) == BIN_10000000)
        {
            trie->current_root->right = interior_node;
        }
        else
        {
            trie->current_root->left = interior_node;
        }
        interior_node->parent = trie->current_root;
    }
    else
    {
        const uint8_t parent_key_bytes_count = parent->key_bits / 8;
        const uint8_t parent_key_bits_mod = parent->key_bits % 8;
        // both of these can not be 0. this should never be true, if it were the root node case should
        // have been triggered
        assert((parent_key_bytes_count != 0 || parent_key_bits_mod != 0));
        uint8_t parent_wild_bit_number = BIN_00000000;
        if (parent_key_bits_mod == 0)
        {
            parent_wild_bit_number = BIN_10000000;
        }
        else if (parent_key_bits_mod == 1)
        {
            parent_wild_bit_number = BIN_01000000;
        }
        else if (parent_key_bits_mod == 2)
        {
            parent_wild_bit_number = BIN_00100000;
        }
        else if (parent_key_bits_mod == 3)
        {
            parent_wild_bit_number = BIN_00010000;
        }
        else if (parent_key_bits_mod == 4)
        {
            parent_wild_bit_number = BIN_00001000;
        }
        else if (parent_key_bits_mod == 5)
        {
            parent_wild_bit_number = BIN_00000100;
        }
        else if (parent_key_bits_mod == 6)
        {
            parent_wild_bit_number = BIN_00000010;
        }
        else if (parent_key_bits_mod == 6)
        {
            parent_wild_bit_number = BIN_00000001;
        }
        bool isParentRight = key[parent_key_bytes_count] & parent_wild_bit_number;
        if (isParentRight)
        {
            parent->right = interior_node;
        }
        else
        {
            parent->left = interior_node;
        }
        interior_node->parent = replacing->parent;
    }
    replacing->parent = interior_node;
    return interior_node;
}

static int _compare_key_bits(const uint256_t a, const uint256_t b, const uint32_t b_key_bits)
{
    const uint32_t bytes_to_check = b_key_bits / 8;
    const uint32_t bits_after_bytes = b_key_bits % 8;
    uint256_t keybit_mask;
    memcpy(keybit_mask, UINT256_ZERO, UINT256_NUM_BYTES);
    uint32_t i = 0;
    for (; i < bytes_to_check; ++i)
    {
        keybit_mask[i] = 255;
    }
    if (bits_after_bytes == 1)
    {
        keybit_mask[i] = BIN_10000000;
    }
    else if(bits_after_bytes == 2)
    {
        keybit_mask[i] = BIN_11000000;
    }
    else if(bits_after_bytes == 3)
    {
        keybit_mask[i] = BIN_11100000;
    }
    else if(bits_after_bytes == 4)
    {
        keybit_mask[i] = BIN_11110000;
    }
    else if(bits_after_bytes == 5)
    {
        keybit_mask[i] = BIN_11111000;
    }
    else if(bits_after_bytes == 6)
    {
        keybit_mask[i] = BIN_11111100;
    }
    else if (bits_after_bytes == 7)
    {
        keybit_mask[i] = BIN_11111110;
    }
    else
    {
        keybit_mask[i] = BIN_00000000;
    }
    uint256_t a_keybits;
    uint256_t b_keybits;
    for (uint32_t j = 0; j < UINT256_NUM_BYTES; ++j)
    {
        a_keybits[j] = a[j] & keybit_mask[j];
        b_keybits[j] = b[j] & keybit_mask[j];
    }
    return memcmp(a_keybits, b_keybits, UINT256_NUM_BYTES);
}

static void _BitwiseTrie_update_fingerprint(TrieNode* node, const BitwiseTrie* trie)
{
    if (trie->hash != NULL)
    {
        trie->hash(node->left->key, node->right->key, node->fingerprint);
    }
}

bool BitwiseTrie_mint(BitwiseTrie* trie,
    const uint256_t key,
    const int64_t* amount,
    const uint8_t* scriptpubkey,
    const uint64_t len_scriptpubkey)
{
    // if trie is null or root is null, bail right away
    if (trie == NULL || trie->current_root == NULL)
    {
        assert(false);
        return false;
    }
    // if scriptpubkey is NULL but has a len, bail due to invalid data
    if (scriptpubkey == NULL && len_scriptpubkey != 0)
    {
        assert(false);
        return false;
    }
    // if scriptpubkey is not NULL but has a len of 0, bail due to invalid data
    if (scriptpubkey != NULL && len_scriptpubkey == 0)
    {
        assert(false);
        return false;
    }
    if (BitwiseTrie_find(trie, key) != NULL)
    {
        // can not add an entry for a value that already exists
        return false;
    }
    if (_BitwiseTrie_check_and_expand_master_list(trie) == false)
    {
        return false;
    }
    // create the new node we will be adding, do it before editing the trie incase we fail to malloc memory
    TrieNode* new_node = malloc(sizeof(TrieNode));
    if (new_node == NULL)
    {
        // failed to malloc memory, return false to indicate failure to add node
        assert(false);
        return false;
    }
    // set the new node to null
    _TrieNode_set_null(new_node, trie);
    if (_TrieNode_create_null_value(new_node) == false)
    {
        _TrieNode_destroy(new_node);
        assert(false);
        return false;
    }
    new_node->value->scriptpubkey = malloc(len_scriptpubkey);
    if (new_node->value->scriptpubkey == NULL)
    {
        // failed to malloc memory, return false to indicate failure to add node
        _TrieNode_destroy(new_node);
        assert(false);
        return false;
    }
    // compare the key of the node being added against the existing nodes starting with the root
    TrieNode* parent = trie->current_root;
    TrieNode* next = trie->current_root;
    bool isLeft = false;
    while (next != NULL)
    {
        if (next->key_bits == 0)
        {
            if ((key[0] & BIN_10000000) == BIN_10000000)
            {
                parent = next;
                next = next->right;
                isLeft = false;
                continue;
            }
            else
            {
                parent = next;
                next = next->left;
                isLeft = true;
                continue;
            }
        }
        // if next is a leaf node...
        if (next->key_bits == 256)
        {
            TrieNode* new_parent_node = _TrieNode_make_new_interior_node(trie, next->parent, next, key);
            if (new_parent_node == NULL)
            {
                // failed to malloc memory, return false to indicate failure to add node
                _TrieNode_destroy(new_node);
                assert(false);
                return false;
            }
            next = new_parent_node;
            parent = new_parent_node->parent;
            continue;
        }
        // next is an interior node
        int32_t res = _compare_key_bits(key, next->key, next->key_bits);
        if (res == 0) // the keybits in the key and next key were the same
        {
            // after the key bits interior node keys are all 0,
            // we only need to check the bit of new node key that is
            // interior node keybits + 1 to determine which way we go.
            // 0 si left, 1 is right
            uint32_t byte_to_check = next->key_bits / 8;
            uint32_t next_key_bits = next->key_bits % 8;
            uint8_t bit_to_check = BIN_10000000;
            if (next_key_bits == 0)
            {
                // intentionally do nothing
            }
            else if (next_key_bits == 1)
            {
                bit_to_check = BIN_01000000;
            }
            else if(next_key_bits == 2)
            {
                bit_to_check = BIN_00100000;
            }
            else if(next_key_bits == 3)
            {
                bit_to_check = BIN_00010000;
            }
            else if(next_key_bits == 4)
            {
                bit_to_check = BIN_00001000;
            }
            else if(next_key_bits == 5)
            {
                bit_to_check = BIN_00000100;
            }
            else if(next_key_bits == 6)
            {
                bit_to_check = BIN_00000010;
            }
            else if (next_key_bits == 7)
            {
                bit_to_check = BIN_00000001;
            }
            res = (key[byte_to_check] & bit_to_check);
            // traversing down in either direction requires the next node to be duplicated for this root group
            // need to make a copy for the current root group
            if (next->root_group != trie->current_root->root_group)
            {
                // the parent will always be in the correct root group
                TrieNode* next_copy = _TrieNode_copy_with_new_parent(next, parent);
                _BitwiseTrie_add_node_to_master_list(trie, next_copy);
                next = next_copy;
            }
            if (res <= 0)
            {
                // we think we want to go left, look left to be sure
                TrieNode* next_left = next->left;
                if (next_left == NULL || next_left->key_bits == 256)
                {
                    parent = next;
                    next = next->left;
                    isLeft = true;
                }
                else
                {
                    int32_t res_go_left = _compare_key_bits(key, next_left->key, next_left->key_bits);
                    if (res_go_left <= 0)
                    {
                        parent = next;
                        next = next->left;
                        isLeft = true;
                    }
                    else // res_go_left > 0
                    {
                        parent = next;
                        next = next->right;
                        isLeft = false;
                    }
                }
            }
            else
            {
                // we think we want to go right, look right to be sure
                TrieNode* next_right = next->right;
                if (next_right == NULL || next_right->key_bits == 256)
                {
                    parent = next;
                    next = next->right;
                    isLeft = false;
                }
                else
                {
                    int32_t res_go_right = _compare_key_bits(key, next_right->key, next_right->key_bits);
                    if (res_go_right <= 0)
                    {
                        parent = next;
                        next = next->left;
                        isLeft = true;
                    }
                    else
                    {
                        parent = next;
                        next = next->right;
                        isLeft = false;
                    }
                }
            }
            continue;
        }
        TrieNode* new_parent_node = _TrieNode_make_new_interior_node(trie, next->parent, next, key);
        if (new_parent_node == NULL)
        {
            // failed to malloc memory, return false to indicate failure to add node
            _TrieNode_destroy(new_node);
            assert(false);
            return false;
        }
        next = new_parent_node;
        parent = new_parent_node->parent;
    }
    // if we are here, next is null. and we need to add data to parent
    // now fill in the new node we want to add
    // set the node's value's data
    memcpy(new_node->key, key, UINT256_NUM_BYTES);
    // leaf node always has 256 key bits
    new_node->key_bits = 256;
    new_node->value->amount = *amount;
    new_node->value->len_scriptpubkey = len_scriptpubkey;
    memcpy(new_node->value->scriptpubkey, scriptpubkey, len_scriptpubkey);
    if (isLeft)
    {
        assert(parent->left == NULL);
        parent->left = new_node;
    }
    else // right
    {
        assert(parent->right == NULL);
        parent->right = new_node;
    }
    new_node->parent = parent;
    _BitwiseTrie_add_node_to_master_list(trie, new_node);
    // now that the new node has been added, go back up to the root and update the fingerprints
    while (parent != NULL)
    {
        _BitwiseTrie_update_fingerprint(parent, trie);
        parent = parent->parent;
    }
    return true;
}

static void _BitwiseTrie_set_null(BitwiseTrie* trie)
{
    // free all of the leaf and interior nodes
    int32_t mem_change = 0;
    uint32_t i;
    for (i = 0; i < trie->len_node_master_list; ++i)
    {
        mem_change = mem_change - _TrieNode_get_memusage(trie->node_master_list[i]);
        _TrieNode_destroy(trie->node_master_list[i]);
    }
    // root nodes are stored in their own array
    for (i = 0; i <= trie->active_root_index; ++i)
    {
        // roots never have a value, to be more efficient we simply subtract
        // sizeof(TrieNode) from mem_change
        mem_change = mem_change - sizeof(TrieNode);
        free(trie->historical_roots[i]);
    }
    // only set len_node_master_list to 0, do not set capacity to 0
    // node_master_list was not freed
    trie->len_node_master_list = 0;
    trie->active_root_index = 0;
    _BitwiseTrie_update_memory_allocated(trie, mem_change);
}

TrieNode* BitwiseTrie_find(BitwiseTrie* trie, const uint256_t key)
{
    // compare the key of the node being added against the existing nodes starting with the root
    TrieNode* next = trie->current_root;
    int res;
    while (next != NULL)
    {
        // special root case
        if (next->key_bits == 0)
        {
            if ((key[0] & BIN_10000000) == BIN_10000000)
            {
                next = next->right;
                continue;
            }
            else
            {
                next = next->left;
                continue;
            }
        }
        if (next->key_bits == 256)
        {
            if (memcmp(next->key, key, UINT256_NUM_BYTES) != 0)
            {
                // arrived at the place where the key should be but it was not found
                return NULL;
            }
            else
            {
                // key was found
                return next;
            }
        }
        res = _compare_key_bits(key, next->key, next->key_bits);
        if (res == 0)
        {
            // the keybits in the key and next key were the same
            // check each branch of the next node
            if (next->left != NULL)
            {
                res = _compare_key_bits(key, next->left->key, next->left->key_bits);
                if (res == 0)
                {
                    next = next->left;
                    continue;
                }
            }
            if (next->right != NULL)
            {
                res = _compare_key_bits(key, next->right->key, next->right->key_bits);
                if (res == 0)
                {
                    next = next->right;
                    continue;
                }
            }
            return NULL;
        }
        else if (res < 0)
        {
            next = next->left;
            continue;
        }
        else // res > 0
        {
            next = next->right;
            continue;
        }
    }
    // this case should never happen, return NULL
    return NULL;
}

bool BitwiseTrie_spend(BitwiseTrie* trie, const uint256_t key)
{
    // if trie is null or root is null, bail right away
    if (trie == NULL || trie->current_root == NULL)
    {
        assert(false);
        return false;
    }
    if (BitwiseTrie_find(trie, key) == NULL)
    {
        // nothing to spend, return false
        // TODO : consider returning true because the state of the trie is
        // the desired end state of the spend
        // could also return the number of elements removed (0 or 1)
        return false;
    }
    if (_BitwiseTrie_check_and_expand_master_list(trie) == false)
    {
        return false;
    }
    // run the find algorithm again but update the nodes we touch
    // compare the key of the node being added against the existing nodes starting with the root
    const uint32_t current_root_group = trie->current_root->root_group;
    TrieNode* parent = trie->current_root;
    TrieNode* next = trie->current_root;
    bool isLeft = false;
    bool removed = false;
    int res;
    while (next != NULL)
    {
        // special root case
        if (next->key_bits == 0)
        {
            if ((key[0] & BIN_10000000) == BIN_10000000)
            {
                parent = next;
                next = next->right;
                isLeft = false;
                continue;
            }
            else
            {
                parent = next;
                next = next->left;
                isLeft = true;
                continue;
            }
        }
        // is next the node we are looking for?
        if (next->key_bits == 256)
        {
            // this is the node we are looking for, disconnect it from its parent in
            // the current root group
            if (isLeft)
            {
                parent->left = NULL;
            }
            else
            {
                parent->right = NULL;
            }
            removed = true;
            break;
        }
        // update every node we touch if they are not in the current root_group
        if (next->root_group != current_root_group)
        {
            // the parent will always be in the correct root group
            TrieNode* next_copy = _TrieNode_copy_with_new_parent(next, parent);
            _BitwiseTrie_add_node_to_master_list(trie, next_copy);
            next = next_copy;
            if (isLeft == true)
            {
                parent->left = next;
            }
            else
            {
                parent->right = next;
            }
        }
        res = _compare_key_bits(key, next->key, next->key_bits);
        if (res == 0)
        {
            // the keybits in the key and next key were the same
            // check each branch of the next node
            if (next->left != NULL)
            {
                res = _compare_key_bits(key, next->left->key, next->left->key_bits);
                if (res == 0)
                {
                    parent = next;
                    next = next->left;
                    isLeft = true;
                    continue;
                }
            }
            if (next->right != NULL)
            {
                res = _compare_key_bits(key, next->right->key, next->right->key_bits);
                if (res == 0)
                {
                    parent = next;
                    next = next->right;
                    isLeft = false;
                    continue;
                }
            }
            break;
        }
        else if (res < 0)
        {
            parent = next;
            next = next->left;
            isLeft = true;
            continue;
        }
        else // res > 0
        {
            parent = next;
            next = next->right;
            isLeft = false;
            continue;
        }
    }
    // if we removed something go back up to the root and trim intermediate nodes with less than 2 children
    if (removed == true)
    {
        TrieNode* parent_copy = parent;
        TrieNode* parent_parent = NULL;
        while(parent != NULL)
        {
            parent_parent = parent->parent;
            if (parent_parent == NULL)
            {
                // only root has a null parent
                // break on root, we can not trim the root
                break;
            }
            int32_t children = 0;
            if (parent->left != NULL)
            {
                children = children + 1;
            }
            if (parent->right != NULL)
            {
                children = children + 2;
            }
            if (children == 3)
            {
                // has both children, we are done
                break;
            }
            else if (children == 0)
            {
                if (parent_parent->left == parent)
                {
                    parent_parent->left = NULL;
                }
                else // parent_parent->right == parent
                {
                    parent_parent->right = NULL;
                }
            }
            else if (children == 1)
            {
                // left child is not null but right is, condense
                if (parent_parent->left == parent)
                {
                    parent_parent->left = parent->left;
                    TrieNode* parent_left = parent->left;
                    parent_left->parent = parent_parent;
                }
                else // parent_parent->right == parent
                {
                    parent_parent->right = parent->left;
                    TrieNode* parent_left = parent->left;
                    parent_left->parent = parent_parent;
                }
            }
            else if (children == 2)
            {
                // right child is not null but left is, condense
                if (parent_parent->left == parent)
                {
                    parent_parent->left = parent->right;
                    TrieNode* parent_right = parent->right;
                    parent_right->parent = parent_parent;
                }
                else // parent_parent->right == parent
                {
                    parent_parent->right = parent->right;
                    TrieNode* parent_right = parent->right;
                    parent_right->parent = parent_parent;
                }
            }
            parent = parent_parent;
        }
        // now that the trie has been updated post spend, update the fingerprints
        while (parent_copy != NULL)
        {
            _BitwiseTrie_update_fingerprint(parent_copy, trie);
            parent_copy = parent_copy->parent;
        }
    }
    return removed;
}

struct TrimStack
{
    TrieNode* node;
    struct TrimStack* next; // next TrimStack node
};
typedef struct TrimStack TrimStack;

static bool _TrimStack_empty(TrimStack* top)
{
    return top == NULL;
}

static void _TrimStack_push(TrimStack** top, TrieNode* node)
{
    TrimStack* new_layer = malloc(sizeof(TrimStack));
    if (new_layer == NULL)
    {
        assert(false);
    }
    new_layer->node = node;
    new_layer->next = (*top);
    (*top) = new_layer;
}

static TrieNode* _TrimStack_pop(TrimStack** top)
{
    assert(_TrimStack_empty(*top) == false);
    TrimStack* tmp = *top;
    TrieNode* res = tmp->node;
    *top = tmp->next;
    free(tmp);
    return res;
}

void BitwiseTrie_trim(BitwiseTrie* trie)
{
    // traverse the entire trie's current root, collect the list of nodes in the trie
    // calloc space for the nodes we find, including the root
    TrieNode** nodes_in_trie = calloc(trie->len_node_master_list, sizeof(TrieNode*));
    size_t i = 0;
    TrieNode* node = trie->current_root;
    // copy the root node into the nodes_in_trie, we always want to keep it and have it at index 0
    nodes_in_trie[i] = _TrieNode_copy(trie->current_root);
    ++i;
    assert(i == 1);
    TrimStack* stack = NULL;
    while (true)
    {
        if (node != NULL)
        {
            _TrimStack_push(&stack, node);
            node = node->left;
        }
        else
        {
            if (_TrimStack_empty(stack) == false)
            {
                node = _TrimStack_pop(&stack);
                // only copy the node if it is a leaf node
                if (node->key_bits == 256)
                {
                    nodes_in_trie[i] = _TrieNode_copy(node);
                    ++i;
                }
                node = node->right;
            }
            else
            {
                break;
            }
        }
    }
    // nodes_in_trie contains a copy of the root node and all of the leaf nodes
    // set the trie to null
    _BitwiseTrie_set_null(trie);
    // set the root first
    trie->current_root = nodes_in_trie[0];
    trie->current_root->root_group = 0;
    trie->current_root->left = NULL;
    trie->current_root->right = NULL;
    // parent should always be null in the root but set it to null for sanity anyway
    trie->current_root->parent = NULL;
    trie->historical_roots[trie->active_root_index] = trie->current_root;
    // add all of the saved leaf nodes back to the trie
    for (size_t j = 1; j < i; ++j)
    {
        TrieNode* n = nodes_in_trie[j];
        BitwiseTrie_mint(trie, n->key, &n->value->amount, n->value->scriptpubkey, n->value->len_scriptpubkey);
        //BitwiseTrie_print_trie(trie);
    }
    // go through the list again and destroy all of the leaf nodes, they have been readded by values
    for (size_t k = 1; k < i; ++k)
    {
        _TrieNode_destroy(nodes_in_trie[k]);
    }
    // the root was not destroyed but is now stored in the new trie
    // free the nodes_in_trie array and return
    free(nodes_in_trie);
}

void BitwiseTrie_destroy(BitwiseTrie* trie)
{
    _BitwiseTrie_set_null(trie);
    free(trie->node_master_list);
    free(trie);
}

void BitwiseTrie_print_trie(BitwiseTrie* trie)
{
    // traverse the entire trie's current root, print key values and keybits
    TrieNode* node = trie->current_root;
    TrimStack* stack = NULL;
    while (true)
    {
        if (node != NULL)
        {
            _TrimStack_push(&stack, node);
            node = node->left;
        }
        else
        {
            if (_TrimStack_empty(stack) == false)
            {
                node = _TrimStack_pop(&stack);
                //if (node->key_bits == 256)
                {
                    printf("node has key[0] %u, node hash key_bits %u \n", node->key[0], node->key_bits);
                }
                node = node->right;
            }
            else
            {
                break;
            }
        }
    }
}

void BitwiseTrie_get_fingerprint(BitwiseTrie* trie, uint256_t res)
{
    memcpy(res, trie->current_root->fingerprint, UINT256_NUM_BYTES);
}
