// Copyright (c) 2022 Dr. Peter R. Rizun
// Copyright (c) 2022 Greg Griffith
// Copyright (c) 2022 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "test_main.h"

static void _test_add_1(BitwiseTrie* trie)
{
    uint256_t key = {127,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    uint8_t scriptpubkey[5] = {0, 1, 2, 3, 4};
    int64_t amount = 10;
    bool res = BitwiseTrie_mint(trie, key, &amount, scriptpubkey, 5);
    assert(res == true);
    assert(trie->current_root->left != NULL);
    assert(trie->current_root->right == NULL);
    assert(trie->len_node_master_list == 1);
    // should have added a node on the left, verify that it has added the correct node information
    TrieNode* root_left = trie->current_root->left;
    assert(root_left->key_bits == 256);
    assert(memcmp(root_left->key, key, UINT256_NUM_BYTES) == 0);
    // because the scriptpubkey and amount are the same for every test, only check it in this
    // first test to check that new nodes correctly contain the node information
    assert(memcmp(root_left->value->scriptpubkey, scriptpubkey, 5) == 0);
    assert(root_left->value->len_scriptpubkey == 5);
    assert(root_left->value->amount == amount);
    printf("Add-1 test passed \n");
}

static void _test_add_2(BitwiseTrie* trie)
{
    uint256_t key = {129,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    uint8_t scriptpubkey[5] = {0, 1, 2, 3, 4};
    int64_t amount = 10;
    bool res = BitwiseTrie_mint(trie, key, &amount, scriptpubkey, 5);
    assert(res == true);
    assert(trie->current_root->left != NULL);
    assert(trie->current_root->right != NULL);
    assert(trie->len_node_master_list == 2);
    // should have added a node on the right, verify that it has added the correct node information
    TrieNode* root_right = trie->current_root->right;
    assert(root_right->key_bits == 256);
    assert(memcmp(root_right->key, key, UINT256_NUM_BYTES) == 0);
    // verify the left has not changed, verify that it has added the correct node information
    TrieNode* root_left = trie->current_root->left;
    assert(root_left->key_bits == 256);
    assert(root_left->key[0] == 127);
    printf("Add-2 test passed \n");
}

static void _test_add_3(BitwiseTrie* trie)
{
    uint256_t key = {131,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    uint8_t scriptpubkey[5] = {0, 1, 2, 3, 4};
    int64_t amount = 10;
    bool res = BitwiseTrie_mint(trie, key, &amount, scriptpubkey, 5);
    assert(res == true);
    assert(trie->current_root->left != NULL);
    assert(trie->current_root->right != NULL);
    // this insertion required adding an internediate node as well as the new node
    assert(trie->len_node_master_list == 4);
    // left should have remained untouched
    TrieNode* root_left = trie->current_root->left;
    assert(root_left->key_bits == 256);
    assert(root_left->key[0] == 127);
    // right should now be an interior node, interior nodes do not have 256 key bits
    TrieNode* root_right = trie->current_root->right;
    assert(root_right->key_bits == 6);
    // not sure how to evaluate the key of interior nodes
    // assert(memcmp(root_right->key, key, UINT256_NUM_BYTES) == 0);
    // the right of the interior node should be the new added node
    TrieNode* newly_added_node = root_right->right;
    assert(newly_added_node->key_bits == 256);
    assert(memcmp(newly_added_node->key, key, UINT256_NUM_BYTES) == 0);
    // the interior left should be the old right node
    TrieNode* old_right_node = root_right->left;
    assert(old_right_node->key_bits == 256);
    assert(old_right_node->key[0] == 129);
    printf("Add-3 test passed \n");
}

static void _test_add_4(BitwiseTrie* trie)
{
    uint256_t key = {130,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    uint8_t scriptpubkey[5] = {0, 1, 2, 3, 4};
    int64_t amount = 10;
    bool res = BitwiseTrie_mint(trie, key, &amount, scriptpubkey, 5);
    assert(res == true);
    assert(trie->current_root->left != NULL);
    assert(trie->current_root->right != NULL);
    // this insertion required adding an internediate node on the right of the current root right as well as the new node
    assert(trie->len_node_master_list == 6);
    // left should have remained untouched
    TrieNode* root_left = trie->current_root->left;
    assert(root_left->key_bits == 256);
    assert(root_left->key[0] == 127);
    // right is still interior node
    TrieNode* root_right = trie->current_root->right;
    assert(root_right->key_bits == 6);
    TrieNode* root_right_left =  root_right->left;
    assert(root_right_left->key_bits == 256);
    assert(root_right_left->key[0] == 129);
    TrieNode* root_right_right = root_right->right;
    // the number of bits in an interior must be more than the number of bits
    // in any interiors closer to the root than it
    assert(root_right_right->key_bits == 7);
    // right right right should be the new old right right
    TrieNode* root_right_right_right = root_right_right->right;
    assert(root_right_right_right->key_bits == 256);
    assert(root_right_right_right->key[0] == 131);
    // right right left should be the new node added this test
    TrieNode* root_right_right_left = root_right_right->left;
    assert(root_right_right_left->key_bits == 256);
    assert(memcmp(root_right_right_left->key, key, UINT256_NUM_BYTES) == 0);
    printf("Add-4 test passed \n");
}

static void _test_add_5(BitwiseTrie* trie)
{
    uint256_t key = {126,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    uint8_t scriptpubkey[5] = {0, 1, 2, 3, 4};
    int64_t amount = 10;
    bool res = BitwiseTrie_mint(trie, key, &amount, scriptpubkey, 5);
    assert(res == true);
    assert(trie->current_root->left != NULL);
    assert(trie->current_root->right != NULL);
    // this insertion required adding an internediate node as well as the new node
    assert(trie->len_node_master_list == 8);
    // added an intermeidate node on the left, moved old left to interior right
    TrieNode* root_left = trie->current_root->left;
    assert(root_left->key_bits == 7);
    TrieNode* root_left_right = root_left->right;
    assert(root_left_right->key_bits == 256);
    assert(root_left_right->key[0] == 127);
    // new node was added on root left left
    TrieNode* root_left_left = root_left->left;
    assert(root_left_left->key_bits == 256);
    assert(memcmp(root_left_left->key, key, UINT256_NUM_BYTES) == 0);
    printf("Add-5 test passed \n");
}

static void _test_add_6(BitwiseTrie* trie)
{
    uint256_t key = {124,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    uint8_t scriptpubkey[5] = {0, 1, 2, 3, 4};
    int64_t amount = 10;
    bool res = BitwiseTrie_mint(trie, key, &amount, scriptpubkey, 5);
    assert(res == true);
    assert(trie->current_root->left != NULL);
    assert(trie->current_root->right != NULL);
    // this insertion required adding an internediate node as well as the new node
    assert(trie->len_node_master_list == 10);
    // interior node at root left was replaced with one with less key bits because
    // a split was needed higher up and was put at the new left's, right. verify this
    TrieNode* root_left = trie->current_root->left;
    // old interior node had 7 key bits, new one is above it so it must have less
    assert(root_left->key_bits == 6);
    // old interior should be at left right
    TrieNode* root_left_right = root_left->right;
    assert(root_left_right->key_bits == 7);
    // and it should have the same children it did before it was moved
    TrieNode* root_left_right_left = root_left_right->left;
    assert(root_left_right_left->key_bits == 256);
    assert(root_left_right_left->key[0] == 126);
    TrieNode* root_left_right_right = root_left_right->right;
    assert(root_left_right_right->key_bits == 256);
    assert(root_left_right_right->key[0] == 127);
    // new node added in this test shoud be on root left left
    TrieNode* root_left_left = root_left->left;
    assert(root_left_left->key_bits == 256);
    assert(memcmp(root_left_left->key, key, UINT256_NUM_BYTES) == 0);
    printf("Add-6 test passed \n");
}

static void _test_add_7(BitwiseTrie* trie)
{
    uint256_t key = {125,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    uint8_t scriptpubkey[5] = {0, 1, 2, 3, 4};
    int64_t amount = 10;
    bool res = BitwiseTrie_mint(trie, key, &amount, scriptpubkey, 5);
    assert(res == true);
    assert(trie->current_root->left != NULL);
    assert(trie->current_root->right != NULL);
    // this insertion required adding an internediate node as well as the new node
    assert(trie->len_node_master_list == 12);
    /*
    // full trie validation, only showing key[0] for brevity
    // trie should have final shape of:
    //                     root = XXXXXXXX
    //                    /               \
    //               011111XX             100000XX
    //              /        |             /      |
    //       0111110X         011111X  10000001    1000001X
    //     /       |          /     |                 /    |
    // 01111100  01111101  01111110  01111111   10000010   10000011
    //
    */
    // check the root first
    TrieNode* root = trie->current_root;
    assert(root->key_bits == 0);
    // now check the right
    TrieNode* root_right = root->right;
    assert(root_right->key_bits == 6);
    assert(root_right->root_group == root->root_group);
    // the first leaf node is root right left
    TrieNode* root_right_left = root_right->left;
    assert(root_right_left->key_bits == 256);
    assert(root_right_left->key[0] == 129);
    assert(root_right_left->left == NULL);
    assert(root_right_left->right == NULL);
    // go right
    TrieNode* root_right_right =  root_right->right;
    assert(root_right_right->key_bits == 7);
    assert(root_right_right->root_group == root->root_group);
    // leaf node 10000010
    TrieNode* root_right_right_left =  root_right_right->left;
    assert(root_right_right_left->key_bits == 256);
    assert(root_right_right_left->key[0] == 130);
    assert(root_right_right_left->left == NULL);
    assert(root_right_right_left->right == NULL);
    TrieNode* root_right_right_right =  root_right_right->right;
    assert(root_right_right_right->key_bits == 256);
    assert(root_right_right_right->key[0] == 131);
    assert(root_right_right_right->left == NULL);
    assert(root_right_right_right->right == NULL);
    // right side verified, verify the left
    TrieNode* root_left = root->left;
    assert(root_left->key_bits == 6);
    assert(root_left->root_group == root->root_group);
    TrieNode* root_left_left = root_left->left;
    assert(root_left_left->key_bits == 7);
    assert(root_left_left->root_group == root->root_group);
    // the two leaf nodes on root left left
    TrieNode* root_left_left_left = root_left_left->left;
    assert(root_left_left_left->key_bits == 256);
    assert(root_left_left_left->key[0] == 124);
    assert(root_left_left_left->left == NULL);
    assert(root_left_left_left->right == NULL);
    TrieNode* root_left_left_right = root_left_left->right;
    assert(root_left_left_right->key[0] == 125);
    assert(root_left_left_right->key_bits == 256);
    assert(root_left_left_right->left == NULL);
    assert(root_left_left_right->right == NULL);
    // go back to root left and go right
    TrieNode* root_left_right = root_left->right;
    assert(root_left_right->key_bits == 7);
    assert(root_left_right->root_group == root->root_group);
    // check the leaf nodes on root left right
    TrieNode* root_left_right_left = root_left_right->left;
    assert(root_left_right_left->key_bits == 256);
    assert(root_left_right_left->key[0] == 126);
    assert(root_left_right_left->left == NULL);
    assert(root_left_right_left->right == NULL);
    TrieNode* root_left_right_right = root_left_right->right;
    assert(root_left_right_right->key_bits == 256);
    assert(root_left_right_right->key[0] == 127);
    assert(root_left_right_right->left == NULL);
    assert(root_left_right_right->right == NULL);

    printf("Add-7 test passed \n");
}


static void _test_add_8(BitwiseTrie* trie)
{
    uint256_t key = {125,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    uint8_t scriptpubkey[5] = {0, 1, 2, 3, 4};
    int64_t amount = 10;
    bool res = BitwiseTrie_mint(trie, key, &amount, scriptpubkey, 5);
    assert(res == false);
}

void test_adds(BitwiseTrie* trie)
{
    // add left
    _test_add_1(trie);
    // add right
    _test_add_2(trie);
    // add right
    _test_add_3(trie);
    // add right
    _test_add_4(trie);
    // add left
    _test_add_5(trie);
    // add left
    _test_add_6(trie);
    // add left
    _test_add_7(trie);
    // add key that already exists
    _test_add_8(trie);
}
