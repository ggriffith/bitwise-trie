// Copyright (c) 2022 Dr. Peter R. Rizun
// Copyright (c) 2022 Greg Griffith
// Copyright (c) 2022 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "test_main.h"

// Note: mem_used is not tested in these tests. the results of that calculation will change slightly
// depending on the hardware. manual testing indicates it is tracking memory usage as intended
int main()
{
    // all test functions are void. if they fail it will assert
    BitwiseTrie* trie = test_create();
    // build a trie
    test_adds(trie);
    // spend the entire trie back to a single root.
    test_spends(trie);
    // destroy the (empty) trie
    BitwiseTrie_destroy(trie);
    // make a new trie
    trie = test_create();
    // build a trie again
    test_adds(trie);
    // start a new root and change the trie.
    test_second_root(trie);
    // trim the trie to the last root
    test_trim(trie);
    // destroy the (populated) trie
    BitwiseTrie_destroy(trie);
    printf("\nALL TESTS PASSED \n");
}
