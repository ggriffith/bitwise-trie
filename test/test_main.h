// Copyright (c) 2022 Dr. Peter R. Rizun
// Copyright (c) 2022 Greg Griffith
// Copyright (c) 2022 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef TEST_TESTMAIN_H
#define TEST_TESTMAIN_H

#include "bitwise_trie.h"

#include <assert.h>

BitwiseTrie* test_create();
void test_adds(BitwiseTrie* trie);
void test_spends(BitwiseTrie* trie);
void test_second_root(BitwiseTrie* trie);
void test_trim(BitwiseTrie* trie);

#endif // TEST_TESTMAIN_H
