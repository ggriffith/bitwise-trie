// Copyright (c) 2022 Dr. Peter R. Rizun
// Copyright (c) 2022 Greg Griffith
// Copyright (c) 2022 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "test_main.h"

static void _test_spend_1(BitwiseTrie* trie)
{
    uint256_t key = {127,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    bool res = BitwiseTrie_spend(trie, key);
    assert(res == true);
    // check the root first
    TrieNode* root = trie->current_root;
    assert(root->key_bits == 0);
    // now check the right
    TrieNode* root_right = root->right;
    assert(root_right->key_bits == 6);
    assert(root_right->root_group == root->root_group);
    assert(root_right->parent == root);
    // the first leaf node is root right left
    TrieNode* root_right_left = root_right->left;
    assert(root_right_left->parent == root_right);
    assert(root_right_left->key_bits == 256);
    assert(root_right_left->key[0] == 129);
    assert(root_right_left->left == NULL);
    assert(root_right_left->right == NULL);
    // go right
    TrieNode* root_right_right =  root_right->right;
    assert(root_right_right->parent == root_right);
    assert(root_right_right->key_bits == 7);
    assert(root_right_right->root_group == root->root_group);
    // leaf node 10000010
    TrieNode* root_right_right_left =  root_right_right->left;
    assert(root_right_right_left->parent == root_right_right);
    assert(root_right_right_left->key_bits == 256);
    assert(root_right_right_left->key[0] == 130);
    assert(root_right_right_left->left == NULL);
    assert(root_right_right_left->right == NULL);
    TrieNode* root_right_right_right =  root_right_right->right;
    assert(root_right_right_right->parent == root_right_right);
    assert(root_right_right_right->key_bits == 256);
    assert(root_right_right_right->key[0] == 131);
    assert(root_right_right_right->left == NULL);
    assert(root_right_right_right->right == NULL);
    // right side verified, verify the left
    TrieNode* root_left = root->left;
    assert(root_left->parent == root);
    assert(root_left->key_bits == 6);
    assert(root_left->root_group == root->root_group);
    TrieNode* root_left_left = root_left->left;
    assert(root_left_left->parent == root_left);
    assert(root_left_left->key_bits == 7);
    assert(root_left_left->root_group == root->root_group);
    // the two leaf nodes on root left left
    TrieNode* root_left_left_left = root_left_left->left;
    assert(root_left_left_left->parent == root_left_left);
    assert(root_left_left_left->key_bits == 256);
    assert(root_left_left_left->key[0] == 124);
    assert(root_left_left_left->left == NULL);
    assert(root_left_left_left->right == NULL);
    TrieNode* root_left_left_right = root_left_left->right;
    assert(root_left_left_right->parent == root_left_left);
    assert(root_left_left_right->key[0] == 125);
    assert(root_left_left_right->key_bits == 256);
    assert(root_left_left_right->left == NULL);
    assert(root_left_left_right->right == NULL);
    // go back to root left and go right, make sure the node was spent and
    // this is now a leaf node
    TrieNode* root_left_right = root_left->right;
    assert(root_left_right->parent == root_left);
    assert(root_left_right->key_bits == 256);
    assert(root_left_right->key[0] == 126);
    assert(root_left_right->right == NULL);
    assert(root_left_right->left == NULL);
    printf("Spend-1 test passed \n");
}

static void _test_spend_2(BitwiseTrie* trie)
{
    uint256_t key = {129,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    bool res = BitwiseTrie_spend(trie, key);
    assert(res == true);
    // check the root first
    TrieNode* root = trie->current_root;
    assert(root->key_bits == 0);
    // now check the right, it should have keybits of 7 now because the left branch of
    // the keybit 6 interior node before it was spent and that keybit6 interior node was
    // trimmed
    TrieNode* root_right = root->right;
    assert(root_right->key_bits == 7);
    assert(root_right->root_group == root->root_group);
    // the first leaf node is root right left
    // leaf node 10000010
    TrieNode* root_right_left =  root_right->left;
    assert(root_right_left->key_bits == 256);
    assert(root_right_left->key[0] == 130);
    assert(root_right_left->left == NULL);
    assert(root_right_left->right == NULL);
    // leaf node 10000011
    TrieNode* root_right_right =  root_right->right;
    assert(root_right_right->key_bits == 256);
    assert(root_right_right->key[0] == 131);
    assert(root_right_right->left == NULL);
    assert(root_right_right->right == NULL);
    // right side verified, verify the left
    TrieNode* root_left = root->left;
    assert(root_left->key_bits == 6);
    assert(root_left->root_group == root->root_group);
    TrieNode* root_left_left = root_left->left;
    assert(root_left_left->key_bits == 7);
    assert(root_left_left->root_group == root->root_group);
    // the two leaf nodes on root left left
    TrieNode* root_left_left_left = root_left_left->left;
    assert(root_left_left_left->key_bits == 256);
    assert(root_left_left_left->key[0] == 124);
    assert(root_left_left_left->left == NULL);
    assert(root_left_left_left->right == NULL);
    TrieNode* root_left_left_right = root_left_left->right;
    assert(root_left_left_right->key[0] == 125);
    assert(root_left_left_right->key_bits == 256);
    assert(root_left_left_right->left == NULL);
    assert(root_left_left_right->right == NULL);
    // go back to root left and go right, make sure the node was spent and
    // this is now a leaf node
    TrieNode* root_left_right = root_left->right;
    assert(root_left_right->key_bits == 256);
    assert(root_left_right->key[0] == 126);
    assert(root_left_right->right == NULL);
    assert(root_left_right->left == NULL);
    printf("Spend-2 test passed \n");
}

static void _test_spend_3(BitwiseTrie* trie)
{
    uint256_t key = {131,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    bool res = BitwiseTrie_spend(trie, key);
    assert(res == true);
    // check the root first
    TrieNode* root = trie->current_root;
    assert(root->key_bits == 0);
    // there is only 1 leaf node on the right side of the tree remaining,
    // it should be directly to the right of root and all interior nodes above it should
    // have been trimmed after this spend
    TrieNode* root_right = root->right;
    assert(root_right->key_bits == 256);
    assert(root_right->key[0] == 130); // leaf node 10000010
    assert(root_right->left == NULL);
    assert(root_right->right == NULL);
    // right side verified, verify the left
    TrieNode* root_left = root->left;
    assert(root_left->key_bits == 6);
    assert(root_left->root_group == root->root_group);
    TrieNode* root_left_left = root_left->left;
    assert(root_left_left->key_bits == 7);
    assert(root_left_left->root_group == root->root_group);
    // the two leaf nodes on root left left
    TrieNode* root_left_left_left = root_left_left->left;
    assert(root_left_left_left->key_bits == 256);
    assert(root_left_left_left->key[0] == 124);
    assert(root_left_left_left->left == NULL);
    assert(root_left_left_left->right == NULL);
    TrieNode* root_left_left_right = root_left_left->right;
    assert(root_left_left_right->key[0] == 125);
    assert(root_left_left_right->key_bits == 256);
    assert(root_left_left_right->left == NULL);
    assert(root_left_left_right->right == NULL);
    // go back to root left and go right, make sure the node was spent and
    // this is now a leaf node
    TrieNode* root_left_right = root_left->right;
    assert(root_left_right->key_bits == 256);
    assert(root_left_right->key[0] == 126);
    assert(root_left_right->right == NULL);
    assert(root_left_right->left == NULL);
    printf("Spend-3 test passed \n");
}

static void _test_spend_4(BitwiseTrie* trie)
{
    uint256_t key = {130,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    bool res = BitwiseTrie_spend(trie, key);
    assert(res == true);
    // check the root first
    TrieNode* root = trie->current_root;
    assert(root->key_bits == 0);
    // all leaf nodes on the right side of the tree have now been spent, root right should
    // now be null
    assert(root->right == NULL);
    // right side verified, verify the left
    TrieNode* root_left = root->left;
    assert(root_left->key_bits == 6);
    assert(root_left->root_group == root->root_group);
    TrieNode* root_left_left = root_left->left;
    assert(root_left_left->key_bits == 7);
    assert(root_left_left->root_group == root->root_group);
    // the two leaf nodes on root left left
    TrieNode* root_left_left_left = root_left_left->left;
    assert(root_left_left_left->key_bits == 256);
    assert(root_left_left_left->key[0] == 124);
    assert(root_left_left_left->left == NULL);
    assert(root_left_left_left->right == NULL);
    TrieNode* root_left_left_right = root_left_left->right;
    assert(root_left_left_right->key[0] == 125);
    assert(root_left_left_right->key_bits == 256);
    assert(root_left_left_right->left == NULL);
    assert(root_left_left_right->right == NULL);
    // go back to root left and go right, make sure the node was spent and
    // this is now a leaf node
    TrieNode* root_left_right = root_left->right;
    assert(root_left_right->key_bits == 256);
    assert(root_left_right->key[0] == 126);
    assert(root_left_right->right == NULL);
    assert(root_left_right->left == NULL);
    printf("Spend-4 test passed \n");
}

static void _test_spend_5(BitwiseTrie* trie)
{
    uint256_t key = {126,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    bool res = BitwiseTrie_spend(trie, key);
    assert(res == true);
    // check the root first
    TrieNode* root = trie->current_root;
    assert(root->key_bits == 0);
    // all leaf nodes on the right side of the tree have now been spent, root right should
    // now be null
    assert(root->right == NULL);
    // right side verified, verify the left
    // it should be a 7 keybit interior node because the 6 keybit interior node only had 1 branch
    // remaining after this spend and should have been trimmed
    TrieNode* root_left = root->left;
    assert(root_left->key_bits == 7);
    assert(root_left->root_group == root->root_group);
    // the two leaf nodes on root left left
    TrieNode* root_left_left = root_left->left;
    assert(root_left_left->key_bits == 256);
    assert(root_left_left->key[0] == 124);
    assert(root_left_left->left == NULL);
    assert(root_left_left->right == NULL);
    TrieNode* root_left_right = root_left->right;
    assert(root_left_right->key[0] == 125);
    assert(root_left_right->key_bits == 256);
    assert(root_left_right->left == NULL);
    assert(root_left_right->right == NULL);
    printf("Spend-5 test passed \n");
}

static void _test_spend_6(BitwiseTrie* trie)
{
    uint256_t key = {124,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    bool res = BitwiseTrie_spend(trie, key);
    assert(res == true);
    // check the root first
    TrieNode* root = trie->current_root;
    assert(root->key_bits == 0);
    // all leaf nodes on the right side of the tree have now been spent, root right should
    // now be null
    assert(root->right == NULL);
    // right side verified, verify the left
    // there is only 1 leaf node remaining, it should be directly on the root left
    TrieNode* root_left = root->left;
    assert(root_left->key[0] == 125);
    assert(root_left->key_bits == 256);
    assert(root_left->left == NULL);
    assert(root_left->right == NULL);
    printf("Spend-6 test passed \n");
}

static void _test_spend_7(BitwiseTrie* trie)
{
    uint256_t key = {125,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    bool res = BitwiseTrie_spend(trie, key);
    assert(res == true);
    // all nodes should be spent, assert right and left of root are both NULL
    assert(trie->current_root->left == NULL);
    assert(trie->current_root->right == NULL);
    printf("Spend-7 test passed \n");
}

void test_spends(BitwiseTrie* trie)
{

    _test_spend_1(trie);

    _test_spend_2(trie);

    _test_spend_3(trie);

    _test_spend_4(trie);

    _test_spend_5(trie);

    _test_spend_6(trie);

    _test_spend_7(trie);
}
