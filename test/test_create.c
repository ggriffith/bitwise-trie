// Copyright (c) 2022 Dr. Peter R. Rizun
// Copyright (c) 2022 Greg Griffith
// Copyright (c) 2022 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "test_main.h"

BitwiseTrie* test_create()
{
    // create the trie
    BitwiseTrie* trie = BitwiseTrie_create(NULL);
    // make sure creation did not fail
    assert(trie != NULL);
    assert(trie->current_root != NULL);
    assert(trie->active_root_index == 0);
    for (uint32_t i = 0; i < MAX_ROOTS_IN_HISTORY; ++i)
    {
        if (i < trie->active_root_index)
        {
            assert(trie->historical_roots[i] != NULL);
        }
        if (i == trie->active_root_index)
        {
            assert(trie->historical_roots[i] == trie->current_root);
        }
        if (i > trie->active_root_index)
        {
            assert(trie->historical_roots[i] == NULL);
        }
    }
    for (uint32_t i = 0; i < DEFAULT_NODE_MASTER_LIST_SIZE; ++i)
    {
        if (i < trie->len_node_master_list)
        {
            assert(trie->node_master_list[i] != NULL);
        }
        else
        {
            assert(trie->node_master_list[i] == NULL);
        }
    }
    assert(trie->current_root->key_bits == 0);
    printf("Create test passed \n");
    return trie;
}
