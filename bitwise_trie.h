// Copyright (c) 2022 Dr. Peter R. Rizun
// Copyright (c) 2022 Greg Griffith
// Copyright (c) 2022 The Bitcoin Unlimited developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITWISETRIE_H
#define BITWISETRIE_H

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __cplusplus
#include <vector>
#endif

#define BITWISE_TRIE_VERSION_MAJOR 0
#define BITWISE_TRIE_VERSION_MINOR 1
#define BITWISE_TRIE_VERSION_PATCH 0

#define UINT256_NUM_BYTES 32

typedef uint8_t uint256_t[UINT256_NUM_BYTES];

static const uint256_t UINT256_ZERO = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
static const uint256_t UINT256_MAX = {255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255};

// used only in leaf nodes
struct TrieEntry // struct is 24 bytes before alignment
{
    int64_t amount; // (8 bytes) CAmount is int64_t in bitcoin code
    uint64_t len_scriptpubkey; // (8 bytes) should be size_t but use uint64_t for consistency across architectures
    uint8_t* scriptpubkey; // (8 bytes pointing to len_scriptpubkey bytes
};
typedef struct TrieEntry TrieEntry;

struct TrieNode // struct is 104 bytes before alignment
{
    struct TrieNode* parent; // 8 bytes
    struct TrieNode* left; // 8 bytes
    struct TrieNode* right; // 8 bytes
    uint32_t root_group; // 4 bytes, denotes which root index this node belongs to. only used in interior nodes
    uint256_t key; // (32 bytes) sha256(txid, index)
    uint32_t key_bits; // (4 bytes) the number of bits used in this key, leaf nodes use 256
    uint256_t fingerprint; // 32 bytes, only used in interior nodes
    // value only used in leaf nodes, interior nodes should be NULL
    TrieEntry* value; // 8 bytes pointing to 24+ bytes (TrieEntry)
};
typedef struct TrieNode TrieNode;

// 64 is an arbitrary value
#define MAX_ROOTS_IN_HISTORY 64
// must be MAX_ROOTS_IN_HISTORY + 1
#define INVALID_ROOT_GROUP (MAX_ROOTS_IN_HISTORY + 1)
// 16384 is an arbitrary value
#define DEFAULT_NODE_MASTER_LIST_SIZE 16384

struct BitwiseTrie // struct is 564 bytes before alignment
{
    TrieNode* current_root; // 8 bytes
    // array of all roots in the trie
    TrieNode* historical_roots[MAX_ROOTS_IN_HISTORY]; // 8 * 64 bytes
    // active index in historical roots the current root is, start at 0, can not be >= MAX_ROOTS_IN_HISTORY
    // we can scan the historical_roots for the last not null index but storing an active index is faster
    uint32_t active_root_index; // 4 bytes
    // because of multiple roots and states, cleaning up the trie using roots would be incredibly messy. Use
    // a master list of all nodes instead. this list's capacity starts arbitrarily at 2^14 (16384) in trie construction
    // if all nodes had all values set then this would be 150K KB of memory
    TrieNode** node_master_list; // 8 * value of capacity_node_master_list
    // holds the len of used indexes the master list, as before with the historical_roots,
    // we can scan for the first NULL value but when adding nodes to the trie, for speed we
    // want to keep track so we can append at the len this list does not include root nodes,
    // those are only stored in the historical_roots array
    uint64_t len_node_master_list; // 8 bytes
    // allocated size of node_master_list
    uint64_t capacity_node_master_list; // 8 bytes
    // pointer to a hash function that takes in 2 keys and returns a 256 bit hash result
    // this function should be collision free
    void(*hash)(const uint256_t a, const uint256_t b, uint256_t res); // 8 bytes
    // the amount of memory used by the trie in bytes
    uint64_t mem_used; // 8 bytes
};
typedef struct BitwiseTrie BitwiseTrie;

// creates a new trie with a defalult value root node if no preif is provided
// we can set len_prefix default value to NULL because NULL == 0 in C
BitwiseTrie* BitwiseTrie_create_with_prefix(const uint8_t* prefix, const size_t len_prefix, void(*hash_function)(const uint256_t a, const uint256_t b, uint256_t res));
BitwiseTrie* BitwiseTrie_create(void(*hash_function)(const uint256_t a, const uint256_t b, uint256_t res));
bool BitwiseTrie_mint(BitwiseTrie* trie, const uint256_t key, const int64_t* amount, const uint8_t* scriptpubkey, const uint64_t len_scriptpubkey);
bool BitwiseTrie_add_new_root(BitwiseTrie* trie);
void BitwiseTrie_set_null(BitwiseTrie* trie);
TrieNode* BitwiseTrie_find(BitwiseTrie* trie, const uint256_t key);
bool BitwiseTrie_spend(BitwiseTrie* trie, const uint256_t key);
void BitwiseTrie_trim(BitwiseTrie* trie);
void BitwiseTrie_destroy(BitwiseTrie* trie);
void BitwiseTrie_print_trie(BitwiseTrie* trie);
void BitwiseTrie_get_fingerprint(BitwiseTrie* trie, uint256_t res);

// a c++ class wrapper around the code. it makes it easier to use in c++
// converting the full trie implementation to c++ would result in very few changes
// making this class wrapper sufficient for the code to be more c++ style
#ifdef __cplusplus

class CBitwiseTrie
{
private:
    BitwiseTrie* trie;

public:

    typedef const TrieEntry* const_iterator;
    typedef TrieEntry* iterator;

    CBitwiseTrie()
    {
        trie = BitwiseTrie_create();
    }
    CBitwiseTrie(const uint8_t* prefix_bytes, const size_t &len_prefix_bytes)
    {
        trie = BitwiseTrie_create_with_prefix(prefix_bytes, len_prefix_bytes);
    }
    CBitwiseTrie(const std::vector<uint8_t>& prefix_bytes)
    {
        trie = BitwiseTrie_create_with_prefix(prefix_bytes.data(), prefix_bytes.size());
    }
    ~CBitwiseTrie()
    {
        BitwiseTrie_destroy(trie);
    }

    bool addNewRoot()
    {
        return BitwiseTrie_add_new_root(trie);
    }

    TrieNode* find(const uint256_t key)
    {
        return BitwiseTrie_find(trie, key);
    }

    bool mint(const uint256_t key, const int64_t &amount, const uint8_t* scriptpubkey, const uint64_t &len_scriptpubkey)
    {
        return BitwiseTrie_mint(trie, key, &amount, scriptpubkey, len_scriptpubkey);
    }

    bool spend(const uint256_t key)
    {
        return BitwiseTrie_spend(trie, key);
    }

    void trim()
    {
        BitwiseTrie_trim(trie);
    }

    void clear()
    {
        BitwiseTrie_set_null(trie);
    }

    void print()
    {
        BitwiseTrie_print_trie(trie);
    }

    void getMemoryUsed()
    {
        return trie->mem_used;
    }

};

#endif // __cplusplus

#endif //BITWISETRIE_H
